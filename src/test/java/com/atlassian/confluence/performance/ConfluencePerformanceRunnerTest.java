package com.atlassian.confluence.performance;

import java.util.logging.Level;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.performance.util.TestConstants.SIZE;
import com.atlassian.confluence.performance.pageobject.Page;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.WebClientProvider;

public class ConfluencePerformanceRunnerTest extends ConfluencePerformanceTestBase {

	@Before
	public void prepare() throws Exception {
		populateWarmUpData();
		doWarmUp();
		populateExecData();
	}

	private void doWarmUp() throws Exception {
		try {
			CustomLogger.getLogger().info("Warming up the instance....");
			loginWarmUp.login();
			spaceCreatorWarmUp.create(true);
			pageCreatorWarmUp.create(true);
			pageCreatorMacrosWarmUp.create(true);
			blogCreatorWarmUp.create(true);
			pageViewerWarmUp.view(50, true);
			pageViewerMacroWarmUp.view(20, true);
			spaceViewerWarmUp.view(50, true);
			spaceBrowserWarmUp.browseSpaceDirectory(10, true);
			dashboardViewerWarmUp.view(50, true);
			searcher.search(1, false);
			logoutWarmUp.logout();
			CustomLogger.getLogger().info("Warming up the instance completed....");
		} catch (Exception e) {
			CustomLogger.getLogger().log(Level.SEVERE, "Error while warm up", e);
		}
	}

	private void populateExecData() {
		populatePageViewer(pageViewer);
		populateCreatePageData(pageCreator, SIZE.SMALL);
		populateCreateBlogData(blogCreator, SIZE.SMALL);
		populateCreateSpaceData(spaceCreator, spaceViewer, SIZE.SMALL);
		populateCreatePageWithMacros(pageCreatorMacros, pageViewerMacro, 2);
		searcher.clear();
		for (Page page : pageViewer.getPages())
			searcher.addSearchKey(page.getTitle());
	}

	private void populateWarmUpData() {
		populatePageViewer(pageViewerWarmUp);
		populateCreatePageData(pageCreatorWarmUp, SIZE.SMALL);
		populateCreateBlogData(blogCreatorWarmUp, SIZE.SMALL);
		populateCreateSpaceData(spaceCreatorWarmUp, spaceViewerWarmUp, SIZE.SMALL);
		populateCreatePageWithMacros(pageCreatorMacrosWarmUp, pageViewerMacroWarmUp, SIZE.SMALL);
		for (Page page : pageViewer.getPages())
			searcher.addSearchKey(page.getTitle());
	}

	@Test
	public void run() throws Exception {
		login.login();
		spaceCreator.create(true);
		pageCreator.create(true);
		pageCreatorMacros.create(true);
		pageViewer.view(500, true);
		spaceViewer.view(500, true);
		pageViewerMacro.view(50, true);
		blogCreator.create(true);
		spaceBrowser.browseSpaceDirectory(200, true);
		dashboardViewer.view(50, true);
		searcher.search(10, true);
		logout.logout();
	}

	@After
	public void afterTest() throws Exception {
		report();
		convertToKotoFormat();
		WebClientProvider.close();
	}
};
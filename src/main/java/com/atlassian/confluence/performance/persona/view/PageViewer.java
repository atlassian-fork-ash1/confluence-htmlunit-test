package com.atlassian.confluence.performance.persona.view;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Page;
import com.atlassian.confluence.performance.persona.Persona;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;
import com.atlassian.confluence.performance.util.TestConstants;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class PageViewer implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();
	private List<Page> pages = new ArrayList<Page>();

	public void view(int repeatition, boolean record) throws Exception {
		while (repeatition > 0) {
			for (Page page : pages) {
				HtmlPage respPage = null;
				try {
					CustomLogger.getLogger().info("Viewing page " + TestConstants.BASE_URL + "display/" + page.getSpaceKey()
							+ "/" + page.getTitle());
					if(record)
						recoder.start();
					respPage = page.view();
				} catch (Exception e) {
					CustomLogger.getLogger().log(Level.SEVERE, "Exception while viewing page : " + TestConstants.BASE_URL + "display/"
							+ page.getSpaceKey() + "/" + page.getTitle(), e);
				} finally {
					if(record)
						recoder.stop();
				}
				CustomLogger.getLogger().info("Response load time : "+respPage.getWebResponse().getLoadTime());
				CustomLogger.getLogger().info("Viewed the page " + page.getSpaceKey() + "/" + page.getTitle());
				Thread.sleep(5);
			}
			repeatition--;
		}
	}

	public void addPage(String spaceKey, String title) {
		this.pages.add(new Page(spaceKey, title));
	}

	public void addAll(List<Page> pages) {
		this.pages.addAll(pages);
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
	
	public void clear(){
		this.pages.clear();
	}
	
	public List<Page> getPages(){
		return this.pages;
	}
}

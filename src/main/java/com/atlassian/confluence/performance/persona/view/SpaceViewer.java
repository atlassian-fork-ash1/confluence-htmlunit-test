package com.atlassian.confluence.performance.persona.view;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Space;
import com.atlassian.confluence.performance.persona.Persona;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;
import com.atlassian.confluence.performance.util.TestConstants;
import com.gargoylesoftware.htmlunit.Page;

public class SpaceViewer implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();
	private List<Space> spaces = new ArrayList<Space>();

	public void view(int repeatition, boolean record) throws Exception {
		while (repeatition > 0) {
			for (Space space : spaces) {
				Page respPage = null;
				try {
					CustomLogger.getLogger().info("Viewing space " + TestConstants.BASE_URL + "display/" + space.getKey());
					if(record)
						recoder.start();
					respPage = space.view();
				} catch (Exception e) {
					CustomLogger.getLogger().log(Level.SEVERE,
							"Exception while viewing space : " + TestConstants.BASE_URL + "display/" + space.getKey(),
							e);
				} finally {
					if(record)
						recoder.stop();
				}
				CustomLogger.getLogger().info("Response load time : " + respPage.getWebResponse().getLoadTime());
				CustomLogger.getLogger().info("Viewed the space " + space.getName());
				Thread.sleep(10);
			}
			repeatition--;
		}
	}

	public void addSpace(String name, String key) {
		this.spaces.add(new Space(name, key));
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}

package com.atlassian.confluence.performance.persona;

public interface Persona {
	public long getMeanInMS();
	public long getMeanInNS();
}

package com.atlassian.confluence.performance.persona.view;

import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Space;
import com.atlassian.confluence.performance.persona.Persona;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;
import com.gargoylesoftware.htmlunit.Page;

public class SpaceBrowser implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();

	public void browseSpaceDirectory(int repeatition, boolean record) throws Exception {
		while (repeatition > 0) {
			Page respPage = null;
			try {
				CustomLogger.getLogger().info("Viewing space directory");
				if(record)
					recoder.start();
				respPage = Space.browseSpaceDirectory();
			} catch (Exception e) {
				CustomLogger.getLogger().log(Level.SEVERE, "Exception while viewing space directory", e);
			} finally {
				if(record)
					recoder.stop();
			}
			CustomLogger.getLogger().info("Response load time : "+respPage.getWebResponse().getLoadTime());
			CustomLogger.getLogger().info("Viewed the space directory.");
			Thread.sleep(10);
			repeatition--;
		}
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}

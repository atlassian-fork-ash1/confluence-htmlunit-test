package com.atlassian.confluence.performance.persona;

import com.atlassian.confluence.performance.pageobject.Interactions;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;
import com.atlassian.confluence.performance.util.TestConstants;

public class Login implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();

	public void login() throws Exception {
		CustomLogger.getLogger().info("Logging in " + TestConstants.BASE_URL);
		recoder.start();
		Interactions.login("admin", "admin");
		recoder.stop();
		CustomLogger.getLogger().info("Logged in.");
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}

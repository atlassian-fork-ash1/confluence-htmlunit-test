package com.atlassian.confluence.performance.persona.create;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Blog;
import com.atlassian.confluence.performance.persona.Persona;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;

public class BlogCreator implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();
	private List<Blog> blogs = new ArrayList<Blog>();

	public void create(boolean record) throws Exception {
		for (Blog blog : blogs) {
			try {
				CustomLogger.getLogger().info("Creating blog " + blog.getSpaceKey() + "/" + blog.getTitle());
				if(record)
					recoder.start();
				blog.create();
			} catch (Exception e) {
				CustomLogger.getLogger().log(Level.SEVERE, "Error while creating a blog", e);
			} finally {
				if(record)
					recoder.stop();
			}
			CustomLogger.getLogger().info("Created the blog " + blog.getSpaceKey() + "/" + blog.getTitle());
			Thread.sleep(100);
		}
	}

	public void addBlog(String spaceKey, String title, String content) {
		this.blogs.add(new Blog(spaceKey, title, content));
	}

	public List<Blog> getBlogs() {
		return this.blogs;
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}
	
	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}

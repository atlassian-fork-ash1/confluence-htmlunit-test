package com.atlassian.confluence.performance.persona.view;

import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Dashboard;
import com.atlassian.confluence.performance.persona.Persona;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;

public class DashboardViewer implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();

	public void view(int repeatition, boolean record) throws Exception {
		while (repeatition > 0) {
			try {
				CustomLogger.getLogger().info("Viewing dashboard and tabs");
				if (record)
					recoder.start();
				Dashboard.view();
				Dashboard.viewAllUpdates();
				Dashboard.viewFavourite();
				Dashboard.viewNetwork();
				Dashboard.viewPopular();
			} catch (Exception e) {
				CustomLogger.getLogger().log(Level.SEVERE, "Exception while viewing dashboard", e);
			} finally {
				if (record)
					recoder.stop();
			}
			CustomLogger.getLogger().info("Viewed the dashboard");
			Thread.sleep(100);
			repeatition--;
		}
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}

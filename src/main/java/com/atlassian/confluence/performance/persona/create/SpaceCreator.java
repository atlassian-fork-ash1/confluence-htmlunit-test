package com.atlassian.confluence.performance.persona.create;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Space;
import com.atlassian.confluence.performance.persona.Persona;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;

public class SpaceCreator implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();
	private List<Space> spaces = new ArrayList<Space>();

	public void create(boolean record) throws Exception {
		for (Space space : spaces) {
			try {
				CustomLogger.getLogger().info("Creating space " + space.getName());
				if(record)
					recoder.start();
				space.create();
			} catch (Exception e) {
				CustomLogger.getLogger().log(Level.SEVERE, "Error while creating a space", e);
			} finally {
				if(record)
					recoder.stop();
			}
			CustomLogger.getLogger().info("Created the space " + space.getName());
			Thread.sleep(1000);
		}
	}

	public void addSpace(String name, String key) {
		this.spaces.add(new Space(name, key));
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}

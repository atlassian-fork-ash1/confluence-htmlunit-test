package com.atlassian.confluence.performance.util;

import java.util.ArrayList;
import java.util.List;

public class PerformanceRecorder {
	private List<Long> start = new ArrayList<Long>();
	private List<Long> end = new ArrayList<Long>();
	private boolean running = false;

	public void start() {
		if (running) {
			throw new IllegalStateException("Recorder has already started.");
		}
		start.add(System.nanoTime());
		running = true;
	}

	public void stop() {
		if (!running) {
			throw new IllegalStateException("Recorder has already stopped.");
		}
		end.add(System.nanoTime());
		running = false;
	}

	public void reset() {
		start.clear();
		end.clear();
		running = false;
	}

	public long getMeanNS() {
		if (running) {
			throw new IllegalStateException("Recorder is running, stop it before getting mean.");
		}
		if (start.size() > 0)
			return (sum(end) - sum(start)) / start.size();
		return 0;
	}

	public long getMeanMS() {
		return getMeanNS() / 1000000;
	}

	private long sum(List<Long> list) {
		long sum = 0;
		for (long l : list)
			sum += l;
		return sum;
	}
}

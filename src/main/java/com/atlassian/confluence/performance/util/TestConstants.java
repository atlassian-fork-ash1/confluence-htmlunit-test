	package com.atlassian.confluence.performance.util;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class TestConstants {
	public static String BASE_URL;
	public static BrowserVersion BROWSER = BrowserVersion.CHROME;

	public static String VIEW_PAGE_INPUT = "";

	public static class SIZE {
		public static final int SMALL = 1;
		public static final int MEDIUM = 10;
		public static final int LARGE = 50;
		public static final int EXTRA_LARGE = 200;
	}

	public static class DATA {
		public static String getRoadMapContent() {
			return "<p><img class=\"editor-inline-macro\" src=\"" + BASE_URL
					+ "plugins/servlet/status-macro/placeholder?title=Status\" data-macro-name=\"status\" data-macro-parameters=\"title=Status\" data-macro-schema-version=\"1\" height=\"18\" width=\"88\"></p><p><img class=\"editor-inline-macro\" "
					+ "src=\"" + BASE_URL
					+ "plugins/servlet/roadmap/image/placeholder?hash=a4adcb70ae9ea97a20ef0960d2a62ec8&amp;width=1000&amp;height=300&amp;timeline=true\" "
					+ "data-macro-name=\"roadmap\" data-macro-parameters=\"hash=a4adcb70ae9ea97a20ef0960d2a62ec8|maplinks=|pagelinks=|source=%7B%22title%22%3A%22Roadmap%20Planner%22%2C%22timeline"
					+ "%22%3A%7B%22startDate%22%3A%222015-09-30%2000%3A00%3A00%22%2C%22endDate%22%3A%222016-08-30%2000%3A00%3A00%22%2C%22displayOption%22%3A%22MONTH%22%7D%2C%22lanes%22%3A%5B%7B%22"
					+ "title%22%3A%22Lane%201%22%2C%22color%22%3A%7B%22lane%22%3A%22%23f6c342%22%2C%22bar%22%3A%22%23fadb8e%22%2C%22text%22%3A%22%23594300%22%2C%22count%22%3A1%7D%2C%22bars%22%3A%5B%7B%22"
					+ "title%22%3A%22Bar%201%22%2C%22description%22%3A%22This%20is%20the%20first%20bar.%22%2C%22startDate%22%3A%222015-09-01%2000%3A00%3A00%22%2C%22duration%22%3A2%2C%22rowIndex"
					+ "%22%3A0%2C%22id%22%3A%2264857657-6e8e-46f3-a416-a1cb8a6701e7%22%2C%22pageLink%22%3A%7B%7D%7D%2C%7B%22title%22%3A%22Bar%202%22%2C%22description%22%3A%22This%20is%20the%20second%20"
					+ "bar.%22%2C%22startDate%22%3A%222015-09-01%2000%3A00%3A00%22%2C%22duration%22%3A1%2C%22rowIndex%22%3A1%2C%22id%22%3A%22d83f34b0-23a3-4b84-b9ae-88512eddd693%22%2C%22pageLink"
					+ "%22%3A%7B%7D%7D%5D%7D%2C%7B%22title%22%3A%22Lane%202%22%2C%22color%22%3A%7B%22lane%22%3A%22%233b7fc4%22%2C%22bar%22%3A%22%236c9fd3%22%2C%22text%22%3A%22%23ffffff%22%2C%22count%22"
					+ "%3A1%7D%2C%22bars%22%3A%5B%7B%22title%22%3A%22Bar%203%22%2C%22description%22%3A%22This%20is%20the%20third%20bar.%22%2C%22startDate%22%3A%222015-09-01%2000%3A00%3A00%22%2C%22duration"
					+ "%22%3A2.5%2C%22rowIndex%22%3A0%2C%22id%22%3A%22615ca2f0-2900-48bd-9a25-7e5b8a497411%22%2C%22pageLink%22%3A%7B%7D%7D%5D%7D%5D%2C%22markers%22%3A%5B%7B%22title%22%3A%22Marker%201%22%2C%22"
					+ "markerDate%22%3A%222015-09-15%2000%3A00%3A00%22%7D%5D%7D|timeline=true|title=Roadmap%20Planner\" data-macro-schema-version=\"1\"></p>";
		}

		public static String getPageTreeContent() {
			return "<p><img class=\"editor-inline-macro\" src=\"" + BASE_URL
					+ "plugins/servlet/status-macro/placeholder?title=Status\" "
					+ "data-macro-name=\"status\" data-macro-id=\"43b83c94-a08e-49b3-b738-5d0848071337\" data-macro-parameters=\"title=Status\" data-macro-schema-version=\"1\" "
					+ "height=\"18\" width=\"88\"></p><p><img class=\"editor-inline-macro\" src=\"" + BASE_URL
					+ "plugins/servlet/confluence/placeholder/macro?"
					+ "definition=e3BhZ2V0cmVlfQ&amp;locale=en_GB&amp;version=2\" data-macro-name=\"pagetree\" data-macro-schema-version=\"1\"></p>";
		}
		
		public static String getMixedContent(){
			return "<table class=\"confluenceTable\"><tbody><tr><th class=\"confluenceTh\">Number</th><th class=\"confluenceTh\">First Name</th><th class=\"confluenceTh\">Last Name</th>"+
					"<th class=\"confluenceTh\">Points</th><th class=\"confluenceTh\" colspan=\"1\" width=\"\">Links</th></tr><tr><td class=\"confluenceTd\">1</td><td class=\"confluenceTd\">"+
					"Eve</td><td class=\"confluenceTd\">Jackson</td><td class=\"confluenceTd\">94</td><td class=\"confluenceTd\" colspan=\"1\" width=\"\"><a href=\"http://www.google.com\">"+
					"Link</a></td></tr><tr><td class=\"confluenceTd\">2</td><td class=\"confluenceTd\">John</td><td class=\"confluenceTd\">Doe</td><td class=\"confluenceTd\">80</td><td class=\"confluenceTd\" "+
					"colspan=\"1\" width=\"\"><a href=\"http://www.atlassian.com\">Atlassian</a></td></tr><tr><td class=\"confluenceTd\">3</td><td class=\"confluenceTd\">Adam</td><td class=\"confluenceTd\">"+
					"Johnson</td><td class=\"confluenceTd\">67</td><td class=\"confluenceTd\" colspan=\"1\" width=\"\"><a href=\"http://cookusart.com/images/1/beautiful-nature/beautiful-nature-02.jpg\">"+
					"Image</a></td></tr><tr><td class=\"confluenceTd\">4</td><td class=\"confluenceTd\">Jill</td><td class=\"confluenceTd\">Smith</td><td class=\"confluenceTd\">50</td>"+
					"<td class=\"confluenceTd\" colspan=\"1\" width=\"\">&nbsp;</td></tr></tbody></table><p>&nbsp;</p><p><img class=\"confluence-embedded-image confluence-external-resource\" "+
					"src=\"http://cookusart.com/images/1/beautiful-nature/beautiful-nature-02.jpg\" data-image-src=\"http://cookusart.com/images/1/beautiful-nature/beautiful-nature-02.jpg\" height=\"250\"></p>";
		}
	}
}

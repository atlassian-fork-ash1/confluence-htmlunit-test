package com.atlassian.confluence.performance.util;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class CustomLogger {
	private static Logger logger;
	
	public static Logger getLogger() throws Exception{
		if(logger == null){
			synchronized (CustomLogger.class) {
				logger = Logger.getLogger("ConfluencePerformanceRunnerTest");
				CustomLogger.logger.setLevel(Level.INFO);
				FileHandler handler = new FileHandler("./target/ConfluencePerformanceRunnerTest.log");
				handler.setFormatter(new SimpleFormatter());
				CustomLogger.logger.addHandler(handler);
			}
		}
		return logger;
	}
}

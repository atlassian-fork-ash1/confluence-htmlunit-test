package com.atlassian.confluence.performance.pageobject;


import java.io.IOException;
import java.net.MalformedURLException;

import com.atlassian.confluence.performance.util.TestConstants;
import com.atlassian.confluence.performance.util.WebClientProvider;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class Page {
	private String spaceKey;
	private String title;
	private String content = "";
	
	public Page(String spaceKey, String title){
		this.spaceKey = spaceKey;
		this.title = title;
	}
	
	public Page(String spaceKey, String title,String content){
		this.spaceKey = spaceKey;
		this.title = title;
		this.content = content;
	}
	
	public static HtmlPage create(String spaceKey, String title, String content) throws Exception{
		HtmlPage page = WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"pages/createpage.action?spaceKey="+spaceKey);
		((HtmlTextInput)page.getElementByName("title")).setValueAttribute(title);
		((HtmlTextArea)page.getElementByName("wysiwygContent")).setText(content);
		return page.getElementByName("confirm").click();
	}
	
	public HtmlPage create() throws Exception{
		HtmlPage page = WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"pages/createpage.action?spaceKey="+spaceKey);
		((HtmlTextInput)page.getElementByName("title")).setValueAttribute(title);
		((HtmlTextArea)page.getElementByName("wysiwygContent")).setText(content);
		return page.getElementByName("confirm").click();
	}
	
	public static HtmlPage view(String spaceKey, String title) throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"display/"+spaceKey+"/"+title);
	}
	
	public HtmlPage view() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"display/"+spaceKey+"/"+title);
	}
	
	public String getSpaceKey(){
		return this.spaceKey;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getContent(){
		return this.content;
	}
}

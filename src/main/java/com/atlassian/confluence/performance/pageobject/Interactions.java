package com.atlassian.confluence.performance.pageobject;


import java.io.IOException;
import java.net.MalformedURLException;

import com.atlassian.confluence.performance.util.TestConstants;
import com.atlassian.confluence.performance.util.WebClientProvider;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class Interactions {
	public static Page login(String userName, String password)
			throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		HtmlPage page = WebClientProvider.getInstance().getPage(TestConstants.BASE_URL + "dologin.action");
		((HtmlTextInput) page.getElementByName("os_username")).setValueAttribute(userName);
		((HtmlPasswordInput) page.getElementByName("os_password")).setValueAttribute(password);
		return page.getElementByName("login").click();
	}
	
	public static Page logout()
			throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL + "logout.action");
	}
	
	public static Page search(HtmlPage page, String queryStr) throws Exception{
		((HtmlTextInput)page.getElementById("quick-search-query")).setValueAttribute(queryStr);
		return page.getElementById("quick-search-submit").click();
	}
	
	public static HtmlPage gotoHome() throws Exception{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL);
	}
}

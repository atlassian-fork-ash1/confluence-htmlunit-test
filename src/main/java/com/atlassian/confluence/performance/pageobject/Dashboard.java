package com.atlassian.confluence.performance.pageobject;


import java.io.IOException;
import java.net.MalformedURLException;

import com.atlassian.confluence.performance.util.TestConstants;
import com.atlassian.confluence.performance.util.WebClientProvider;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;

public class Dashboard {
	public static Page view() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"dashboard.action");
	}
	
	public static Page viewPopular() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"dashboard.action?updatesSelectedTab=popular");
	}
	
	public static Page viewAllUpdates() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"dashboard.action?updatesSelectedTab=all");
	}
	
	public static Page viewFavourite() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"dashboard.action?updatesSelectedTab=my");
	}
	
	public static Page viewNetwork() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"dashboard.action?updatesSelectedTab=network");
	}
}

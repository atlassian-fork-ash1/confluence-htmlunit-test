package com.atlassian.confluence.performance.pageobject;


import java.io.IOException;
import java.net.MalformedURLException;

import com.atlassian.confluence.performance.util.TestConstants;
import com.atlassian.confluence.performance.util.WebClientProvider;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class Space {
	private String name;
	private String key;
	
	public Space(String name, String key){
		this.name = name;
		this.key = key;
	}
	
	public static Page create(String name, String key) throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		HtmlPage page = WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"spaces/createspace-start.action");
		((HtmlTextInput)page.getElementByName("name")).setValueAttribute(name);
		((HtmlTextInput)page.getElementByName("key")).setValueAttribute(key);
		return page.getElementByName("create").click();
	}
	
	public Page create() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		HtmlPage page = WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"spaces/createspace-start.action");
		((HtmlTextInput)page.getElementByName("name")).setValueAttribute(name);
		((HtmlTextInput)page.getElementByName("key")).setValueAttribute(key);
		return page.getElementByName("create").click();
	}
	
	public static Page browseSpaceDirectory() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"spacedirectory/view.action");
	}
	
	public static Page view(String key) throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"display/"+key);
	}
	
	public Page view() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		return WebClientProvider.getInstance().getPage(TestConstants.BASE_URL+"display/"+key);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
